package SistemaLDE;

import javax.swing.JOptionPane;

public class Principal {
    

    public static void main(String[] args) {

        /*-----------------------------------
                  ATRIBUTOS DA CLASSE
          -----------------------------------*/
        
        int opcao;   // Opção para trabalhar o menu principal.
        
        int cdc;     // Código do Candidato.
        String ndc = "";  // Nome do Candidato.
        int pdcnl;   // Posição do Candidato na lista.
        int idc = 0;     //Idade do candidato
        int rmdc = 0;    //Renda media 
        int pdc = 0;
        int ncdc;    // Novo código do Candidato.
        String nndc; // Novo nome do Candidato.
        int npdcnl = 0;   // Posição do Candidato na lista.
        int nidc = 0;     //Idade do candidato
        int nrmdc = 0;    //Renda media 
        int npdc = 0;
        
        String tituloDasTelas = "Sistema LDE";
        
        // Atributo para manipular o descritor e as operações da lista.
        EstruturaDeDados LDE;
        
        /*-----------------------------------
                  INSTÂNCIAS DE CLASSE
          -----------------------------------*/
        
        LDE = new EstruturaDeDados();
        
        /*-----------------------------------
                   MENU DE OPERAÇÕES       
          -----------------------------------*/
        
        do{
            
            opcao = menuPrincipal();
            
            switch(opcao){
                
               // case 0: LDE.limparLista();
                 //       JOptionPane.showMessageDialog(null,"Tchau...",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                   //     break;
                
                case 1: LDE.inicializarLista();
                        JOptionPane.showMessageDialog(null, "Candidato criada com sucesso!",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        break;
                        
                case 2:	cdc   = Integer.parseInt(JOptionPane.showInputDialog(null,"Código do candidato: ",tituloDasTelas,JOptionPane.PLAIN_MESSAGE));
                        ndc   = JOptionPane.showInputDialog(null,"Nome do candidato: ",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        idc   = Integer.parseInt(JOptionPane.showInputDialog(null,"Idade do candidato: ",tituloDasTelas,JOptionPane.PLAIN_MESSAGE));
                        rmdc  = Integer.parseInt(JOptionPane.showInputDialog(null,"Renda do candidato: ",tituloDasTelas,JOptionPane.PLAIN_MESSAGE));
                        pdc   = Integer.parseInt(JOptionPane.showInputDialog(null,"Pontuação do candidato: ",tituloDasTelas,JOptionPane.PLAIN_MESSAGE));
                        
                        
                        pdcnl = Integer.parseInt(JOptionPane.showInputDialog(null,"Posição do Candidato na LDE: ",tituloDasTelas,JOptionPane.PLAIN_MESSAGE));
                        
                        if (LDE.inserirCandidato(cdc, ndc, idc, pdc, rmdc,pdcnl))
                            JOptionPane.showMessageDialog(null,"Candidato " + ndc + " inserido!",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        else
                            JOptionPane.showMessageDialog(null,"Candidato " + ndc + " não inserido ou já presente na lista!",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        
                        break;

                case 3:	pdcnl = Integer.parseInt(JOptionPane.showInputDialog(null,"Posição do candidato na LDE: ",tituloDasTelas,JOptionPane.PLAIN_MESSAGE));
                        
                        if (LDE.removerCandidato(pdcnl))
                            JOptionPane.showMessageDialog(null,"Candidato removido!",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        else
                            JOptionPane.showMessageDialog(null,"Candidato não removido ou não está presente na LDE!",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        
                        break;

                case 4:  ndc = JOptionPane.showInputDialog(null,"Nome do usuário: ",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        
                        if( LDE.pesquisarCandidatos(ndc) != null )
                            JOptionPane.showMessageDialog(null,"Candidato encontrado!",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        else
                            JOptionPane.showMessageDialog(null,"Candidato não encontrado ou não existe(m) candidato(s) na LDE!",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                       
                        break;
                        
                            //Alterar Candidato
                case 5: ncdc = Integer.parseInt(JOptionPane.showInputDialog(null,"Código do novo Candidato: ",tituloDasTelas,JOptionPane.PLAIN_MESSAGE));
                        nndc = JOptionPane.showInputDialog(null,"Nome do novo candidato: ",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        nidc = Integer.parseInt(JOptionPane.showInputDialog(null,"Idade do novo Candidato: ",tituloDasTelas,JOptionPane.PLAIN_MESSAGE));
                        npdc = Integer.parseInt(JOptionPane.showInputDialog(null,"Pontuação do novo Candidato: ",tituloDasTelas,JOptionPane.PLAIN_MESSAGE));
                        nrmdc = Integer.parseInt(JOptionPane.showInputDialog(null,"Renda do novo Candidato: ",tituloDasTelas,JOptionPane.PLAIN_MESSAGE));
                        
                        ndc = JOptionPane.showInputDialog(null,"Nome do candidato que deseja alterar: ",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        
                        if( !"".equals(LDE.alterarDadosDoCandidato(ncdc,nidc,npdc,nrmdc,nndc,ndc)))
                        
                        //if( LDE.alterarDadosDoCandidato(ncdc,nidc,npdc,nrmdc,nndc,ndc))
                            JOptionPane.showMessageDialog(null,"Dados alterados com sucesso!",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        else
                            JOptionPane.showMessageDialog(null,"Erro ao alterar os dados ou não existe(m) usuário(s) na LDE!",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        
                        break;


                
                case 6: String relatorio = LDE.relatorio();
                        
                        if( !"".equals(relatorio) )
                            JOptionPane.showMessageDialog(null,relatorio,tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                        else
                            JOptionPane.showMessageDialog(null,"Não existe(m) candidato(s) cadastrado(s)!",tituloDasTelas,JOptionPane.PLAIN_MESSAGE);
                            
                        break;
                
                default: JOptionPane.showMessageDialog(null,"Opção inválida!","Advertência",JOptionPane.WARNING_MESSAGE);
                
            }
            
        }while(opcao != 0);

    }
    
    // Método para acessar o menu principal da aplicação
    private static int menuPrincipal(){
        
        return Integer.parseInt(JOptionPane.showInputDialog(null, 
                                                                  "=======================         \n"
                                                                + " 1 - Criar lista                \n"
                                                                + " 2 - Inserir candidato          \n"
                                                                + " 3 - Excluir Candidato          \n"
                                                                + " 4 - Pesquisar candidato        \n"
                                                                + " 5 - Alterar dados do candidato \n"
                                                                + " 6 - Relatório de dados         \n"
                                                                + " 0 - Sair da aplicação          \n"
                                                                + "=======================         \n"
                                                                + "    Digite uma opção válida     \n"
                                                                + "=======================         \n",
                                                                     
                                                                "Sistema LDE", JOptionPane.QUESTION_MESSAGE));
        
    }
   
}

