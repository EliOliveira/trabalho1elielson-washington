package SistemaLDE;

/**
 *
 * @author elielson.oliveira
 */

public class Candidato {
 
 private int CodigoDoCandidato;
 private String NomeDoCandidato;
 private int IdadeDoCandidato;
 private int PontuacaoDoCandidato;
 private int RendaMensalDoCandidato;

    
    public Candidato(){
    
}    
    /**
     * 
     * @param _cdc Código inscrição do candidato
     * @param _ndc  Nome do candidato
     * @param _idc  Idade do candidato
     * @param _pdc  Pontuação do candidato
     * @param _rmdc Renda Mensal do Candidato
     * 
     */
    
       public Candidato(int _cdc, String _ndc, int _idc, int _pdc, int _rmdc){
           this.setCodigoDoCandidato(_cdc);
           this.setIdadeDoCandidato(_idc);
           this.setNomeDoCandidato(_ndc);
           this.setPontuacaoDoCandidato(_pdc);
           this.setRendaMensalDoCandidato(_rmdc);
    
}  
     public int getCodigoDoCandidato() {
        return this.CodigoDoCandidato;
    }

    public void setCodigoDoCandidato(int _cdc ) {
        this.CodigoDoCandidato = _cdc;
    }

    public String getNomeDoCandidato() {
        return this.NomeDoCandidato;
    }

    public void setNomeDoCandidato(String _ndc) {
        this.NomeDoCandidato = _ndc;
    }

    public int getIdadeDoCandidato() {
        return this.IdadeDoCandidato;
    }

    public void setIdadeDoCandidato(int _idc) {
        this.IdadeDoCandidato = _idc;
    }

    public int getPontuacaoDoCandidato() {
        return this.PontuacaoDoCandidato;
    }

    public void setPontuacaoDoCandidato(int _pdc) {
        this.PontuacaoDoCandidato = _pdc;
    }

    public int getRendaMensalDoCandidato() {
        return this.RendaMensalDoCandidato;
    }

    public void setRendaMensalDoCandidato(int _rmdc) {
        this.RendaMensalDoCandidato = _rmdc;
    }
    

}