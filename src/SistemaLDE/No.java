package SistemaLDE;

/**
 *
 * @author elielson.oliveira
 */
public class No {

    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    private No anterior;
    private Candidato dadosDoCandidato;
    private No proximo;
    
    /*-----------------------------------
            CONSTRUTORES DA CLASSE
      -----------------------------------*/
    
    public No(){
        
    }
    
    public No(int _cdc, String _ndc, int _idc, int _pdc, int _nmdc){
        
        this.setAnterior(null);
        
        Candidato _c = new Candidato(_cdc, _ndc, _idc, _pdc, _nmdc);
        this.setDadosDoCandidato(_c);
        
        this.setProximo(null);
        
    }
    
    /*-----------------------------------
           MÉTODOS get E set DA CLASSE
      -----------------------------------*/
    
    public No getAnterior() {
        return this.anterior;
    }

    public void setAnterior(No _a) {
        this.anterior = _a;
    }

    public Candidato getDadosDoCandidato() {
        return this.dadosDoCandidato;
    }

    public void setDadosDoCandidato(Candidato _ddc) {
        this.dadosDoCandidato = _ddc;
    }

    public No getProximo() {
        return this.proximo;
    }

    public void setProximo(No _p) {
        this.proximo = _p;
    }

}