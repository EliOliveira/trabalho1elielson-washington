 package SistemaLDE;

/**
 *
 * @author elielson.oliveira
 */

public class EstruturaDeDados {

    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    private No inicioDaLista;
    private int quantidadeDeCandidatos;
    private No finalDaLista;
    
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    public EstruturaDeDados(){ }

    /*-----------------------------------
           MÉTODOS get E set DA CLASSE
      -----------------------------------*/

    public No getInicioDaLista() {
        return this.inicioDaLista;
    }

    public void setInicioDaLista(No _idl) {
        this.inicioDaLista = _idl;
    }
    
    public int getQuantidadeDeCandidatos() {
        return this.quantidadeDeCandidatos;
    }

    public void setQuantidadeDeCandidatos(int _qdc) {
        this.quantidadeDeCandidatos = _qdc;
    }

    public No getFinalDaLista() {
        return this.finalDaLista;
    }

    public void setFinalDaLista(No _fdl) {
        this.finalDaLista = _fdl;
    }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/
    
    // Método que inicializa os valores do atributos da classe,
    // ou seja, inicializa os valores do nó descritor da LDE.
    public void inicializarLista(){
        
        this.setInicioDaLista(null);
        this.setQuantidadeDeCandidatos(0);
        this.setFinalDaLista(null);
        
    }
    
    // Método que verifica se a LDE está vazia
    private boolean isEmpty(){
        
        return this.getQuantidadeDeCandidatos() == 0;
        
    }
    
    // Método para inserir um usuário na LDE de acordo com uma posição válida.
    public boolean inserirCandidato(int _cdc, String _ndc, int _idc, int _pdc, int _rmdc, int _p){
        
        // Verifica se é uma posição inválida.
        if( (_p < 0) || (_p > this.getQuantidadeDeCandidatos()) )
            
            return false;
        
        else{
            
            // Cria um novo nó e já insere os dados do usuário.
            No novoNo = new No(_cdc, _ndc, _idc, _pdc, _rmdc);
            
            // Caso 1 -> Lista vazia.
            if ( this.isEmpty() ){
                
                this.setInicioDaLista(novoNo); // Nó criado é o 1º
                this.setFinalDaLista(novoNo);  // e último da LDE.
                
            }else{
                
                // Caso 2 -> Posição = 0
                if ( _p == 0 ){
                    
                    // Novo nó vai apontar para onde o início da lista está apontando.
                    novoNo.setProximo(this.getInicioDaLista());
                    
                    // O 1º nó atual da LDE aponta para o novo nó.
                    this.getInicioDaLista().setAnterior(novoNo);
                    this.setInicioDaLista(novoNo); // O novo nó passa a ser o 1º nó da LDE.
                    
                }else{
                    
                    // Caso 3 -> Posição = quantidade de usuarios.
                    if ( _p == this.getQuantidadeDeCandidatos() ){
                        
                        // Novo nó vai apontar para onde o final da lista está apontando.
                        novoNo.setAnterior(this.getFinalDaLista());
                        
                        // O último nó da LDE aponta para o novo nó.
                        this.getFinalDaLista().setProximo(novoNo);
                        this.setFinalDaLista(novoNo); // O novo nó passa a ser o último nó da LDE.
                        
                    }else{
                        
                        // Caso 4 -> [ p > 0 ] e [ p < quantidade de usuarios ]
                        
                        No usuarioAtual, proximoUsuario;
                        int contador;
                        
                        // Verificando se é melhor percorrer a LDE pelo:
                        // Início ou Final da LDE.
                        
                        // Se a posição for menor ou igual à descoberta da metade da lista,
                        // então:
                        if( _p <= (this.getQuantidadeDeCandidatos()/2) ){
                            
                            // Percorrer começando do início.
                            usuarioAtual = this.getInicioDaLista();
                            contador = 0;
                            
                            // Enquanto não chega a uma posição anterior a que se quer inserir,
                            // vai para o próximo usuário.
                            while( contador < (_p-1) ){
                                
                                usuarioAtual = usuarioAtual.getProximo(); // Indo para o próximo usuário.
                                contador++;
                            
                            }
                            
                            // Determina quem é o próximo usuário.
                            proximoUsuario = usuarioAtual.getProximo();
                            
                            novoNo.setAnterior(usuarioAtual);
                            novoNo.setProximo(proximoUsuario);
                            
                            usuarioAtual.setProximo(novoNo);
                            proximoUsuario.setAnterior(novoNo);
                                                        
                        }else{
                            
                            // Percorrer começando do final.                            
                            usuarioAtual = this.getFinalDaLista();
                            contador = this.getQuantidadeDeCandidatos()-1;
                            
                            // Enquanto não chega a uma posição anterior a que se quer inserir,
                            // vai para o usuário anterior.
                            while( contador > _p ){
                                
                                usuarioAtual = usuarioAtual.getAnterior(); // Indo para o próximo usuário.
                                contador--;
                           
                            }
                            
                            // Determina quem é o próximo usuário.
                            proximoUsuario = usuarioAtual.getAnterior();
                            
                            novoNo.setAnterior(proximoUsuario);
                            novoNo.setProximo(usuarioAtual);
                            
                            proximoUsuario.setProximo(novoNo);
                            usuarioAtual.setAnterior(novoNo);
                            
                        }
                        
                    }
                                                            
                }
                
            }
            
            // Atualizando a quantidade de usuários.
            this.setQuantidadeDeCandidatos(this.getQuantidadeDeCandidatos()+1);
            return true;
            
        }
        
    }
    
    // Método para remover um usuário da LDE de acordo com uma posição válida.
    public boolean removerCandidato(int _p){
       
        // Verifica se é uma posição inválida.
        if( (_p < 0) || (_p >= this.getQuantidadeDeCandidatos()) )
           
            return false;
       
        else{
           
            // Caso 1: LDE possui apenas 1 usuário.
            if ( this.getQuantidadeDeCandidatos() == 1 ){
               
                // Inicializa a LDE.
                this.inicializarLista();
               
            }else{
               
                
                No usuarioAtual;

                // Caso 2: p = 0.
                if ( _p == 0 ){
                    
                    // Usuário atual aponta para o 2º nó da LDE.
                    usuarioAtual = this.getInicioDaLista().getProximo();
                    
                    // O 1º nó não aponta mais para o 2º nó da LDE.
                    this.getInicioDaLista().setProximo(null);

                    // O 2º nó não aponta mais para o 1º nó da LDE.
                    usuarioAtual.setAnterior(null);

                    // O 2º nó da LDE passa a ser o 1º.
                    this.setInicioDaLista(usuarioAtual);

                }else{

                    // Caso 3 : última posição
                    if( _p == (this.getQuantidadeDeCandidatos()-1) ){

                        // Usuário atual aponta para o penúltimo nó da LDE.
                        usuarioAtual = this.getFinalDaLista().getAnterior();

                        // O último nó não aponta mais para o penúltimo nó da LDE.
                        this.getFinalDaLista().setAnterior(null);

                        // O penúltimo nó não aponta mais para o último nó da LDE.
                        usuarioAtual.setProximo(null);

                        // O penúltimo nó da LDE passa a ser o último.
                        this.setFinalDaLista(usuarioAtual);

                    }else{

                        int contador;

                        No proximoUsuario;

                        // Caso 4 -> [ p > 0 ] e [ p < quantidade de usuarios ]
                        
                        // Verificando se é melhor percorrer a LDE pelo:
                        // Início ou Final da LDE.
                        
                        // Se a posição for menor ou igual à descoberta da metade da lista,
                        // então:
                        if( _p <= (this.getQuantidadeDeCandidatos() / 2) ){

                            // Percorrer começando do início.
                            usuarioAtual   = this.getInicioDaLista();
                            
                            // Registrando quem é o usuário depois de 'usuarioAtual'.
                            proximoUsuario = usuarioAtual.getProximo();
                            
                            contador = 0;
                            
                            // Enquanto não chega a uma posição anterior a que se quer inserir,
                            // vai para o próximo usuário.
                            while( contador < (_p-1) ){
                                
                                usuarioAtual = usuarioAtual.getProximo(); // Indo para o próximo usuário.
                                contador++;
                            
                            }
                            
                            // Indo para o próximo usuário.
                            // Nesse caso, registra o usuário que vem depois do usuário que se quer remover.
                            proximoUsuario = proximoUsuario.getProximo();

                            usuarioAtual.getProximo().setAnterior(null);

                            proximoUsuario.getAnterior().setProximo(null);

                            usuarioAtual.setProximo(proximoUsuario);

                            proximoUsuario.setAnterior(usuarioAtual);

                        }else{

                            // Percorrer começando do final.
                            
                            // Implementar o código do final para o início da lista.

                        }

                    }

                }
               
            }
           
            // Atualizando a quantidade de usuários.
            this.setQuantidadeDeCandidatos(this.getQuantidadeDeCandidatos()-1);
            return true;
            
        }
        
    }
    
    // Método para pesquisar um determinado usuário na lista.
    public No pesquisarCandidatos(String _ndc){
        
        // Inicia no endereço do primeiro nó.
        No usuarioAtual = this.getInicioDaLista();
        
        // Enquanto o nó atual for diferente de null,
        while( usuarioAtual != null ){
            
            // vai comparando o código do usuário a ser alterado '_ca'
            // com o código do usuário do nó atual 'usuarioAtual'.
            if ( _ndc.equals(usuarioAtual.getDadosDoCandidato().getNomeDoCandidato()))
                return usuarioAtual; // Caso encontre, retorna o endereço do 'usuarioAtual'.
           
            else
             usuarioAtual = usuarioAtual.getProximo(); // Caso contrário, vá para o próximo usuário.
            
            }     
       
        return null; // Caso não encontre o usuário, retorna null.
        
    }
    
    // Método para alterar os dados de um determinado usuário.
    public boolean alterarDadosDoCandidato(int _codi, int _idad, int _pont, int _rend, String _nome,String n){
        
        // Primeiro, pesquisa pelo código do usuário '_ca' que se deseja alterar.
        // Caso o código do usuário exista, armazena o endereço dele em 'enderecoEncontrado'.
        No enderecoEncontrado = this.pesquisarCandidatos(n);
        
        // Verificando se realmente o usuário foi encontrado
        // Caso sim, com o endereço do nó corrente, basta acessar os atributos da
        // classe Usuário e alterar por '_nc' e '_nn'.
        if ( enderecoEncontrado != null ){
            
            // Alterando os dados do usuário antigo pelos dados do usuário atual.
            enderecoEncontrado.getDadosDoCandidato().setCodigoDoCandidato(_codi);
            enderecoEncontrado.getDadosDoCandidato().setNomeDoCandidato(_nome);
            enderecoEncontrado.getDadosDoCandidato().setIdadeDoCandidato(_idad);
            enderecoEncontrado.getDadosDoCandidato().setRendaMensalDoCandidato(_rend);
            enderecoEncontrado.getDadosDoCandidato().setPontuacaoDoCandidato(_pont);
            
            return true;
            
        }
        
        return false;
        
    }
 
    
    // Método para armazenar os dados de todos os usuários em uma String
    // e retornar para a classe 'Principal.java'.
    public String relatorio(){
        
        String ddtu = ""; // ddtu = dadosDeTodosOsUsuarios
        
        // Se a lista nãi estiver vazia
        if ( !this.isEmpty() ){
            
            No usuarioAtual  = this.getInicioDaLista(); // Inicia no endereço do primeiro nó.
            int posicaoAtual = 0; // Primeira posição da lista
            No usuarioAux;
            
            
            
            ddtu += "-------------------------------------------------------\n";
            ddtu += "Posição | Código | Nome  |  idade  | Pontuação | Renda \n";
            ddtu += "-------------------------------------------------------\n";
           
            // Enquanto o nó atual for diferente de null, vai coletando os dados e armazenando na String.
            while ( usuarioAtual != null ){
                
                ddtu += "       " + posicaoAtual + "       | ";
                ddtu += "    " + Integer.toString(usuarioAtual.getDadosDoCandidato().getCodigoDoCandidato()) + "        | ";
                ddtu += " " + usuarioAtual.getDadosDoCandidato().getNomeDoCandidato() + "        | ";
                ddtu += "    " + Integer.toString(usuarioAtual.getDadosDoCandidato().getIdadeDoCandidato()) + "        | ";
                ddtu += "    " + Integer.toString(usuarioAtual.getDadosDoCandidato().getPontuacaoDoCandidato()) + "        | ";
                ddtu += "    " + Integer.toString(usuarioAtual.getDadosDoCandidato().getRendaMensalDoCandidato()) + "\n";
                    
                            
                 usuarioAtual = usuarioAtual.getProximo(); // Indo para o próximo usuário
                 posicaoAtual++; // Atualizando a posição
                 
                
                 
                 
            }
        
            
            ddtu += "-------------------------------------------------------\n";
            
        }
        
        return ddtu;
        
    }
    
    
    // Método para limpar a lista
    public void limparLista(){
        
        //Inicializa a LDE.
      this.inicializarLista();
        
    }
}
